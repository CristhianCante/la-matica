let mymap = L.map('mapid');
mymap.setView([4.627447191800487,-74.1682505607605], 16);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
miProveedor.addTo(mymap);
var greenIcon = L.icon({
    iconUrl : 'leaf-green.png' ,
    shadowUrl: 'leaf-shadow.png',
    
    iconSize :    [38, 95],
    shadowSize:   [50, 64],
    iconAnchor:   [22, 94],
    shadowAnchor: [4, 62], 
    popupAnchor:  [-3, -76]
})
let lamatica = L.marker([4.626965967767793, -74.1675317287445],{icon: greenIcon} )
lamatica.addTo(mymap)
lamatica.bindPopup("aqui esta la matica").openPopup()
 
